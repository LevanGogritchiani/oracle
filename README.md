create or replace package DEPT_PACK is

  -- Author  : USER
  -- Created : 5/19/2024 4:59:34 PM
  -- Purpose : 
  
  type dep_emp_rec is table of employees%rowtype index by pls_integer;
  
  
  type gen_info_rec is record(
        DEPARTMENT_NAME  departments.department_name%type,
        MANAGER_ID       employees.manager_id%type,
        LOCATION_ID      departments.location_id%type
       );
       
   function check_new_dept(P_DEPARTMENT_NAME  departments.department_name%type,
                                P_MANAGER_ID  employees.manager_id%type,
                              P_LOCATION_ID  departments.location_id%type) return number;
                              
   function gen_new_dept_id return number;                           
                              
    procedure add_dept(P_DEPARTMENT_NAME  departments.department_name%type,
                                P_MANAGER_ID  employees.manager_id%type,
                              P_LOCATION_ID  departments.location_id%type);  
   function get_dept_full_info(p_id number,P_dep_emp out dep_emp_rec) return gen_info_rec ; 
   procedure modify_dept(p_dept_id number,
                        p_action varchar2,
                        p_dept_name varchar2);                                                    

end DEPT_PACK;



-000000000000000000000000000000000000000000000000000000000000000000000000000000000

create or replace package body DEPT_PACK is
   
   --
   function gen_new_dept_id return number is
   begin
     return dep_seq.nextval;
   end;
   --
   function check_new_dept(P_DEPARTMENT_NAME  departments.department_name%type,
                                P_MANAGER_ID  employees.manager_id%type,
                              P_LOCATION_ID  departments.location_id%type) return number is
   
   cursor c_check_coll is
    select count(1)
  from employees b
  where b.department_id in (10,40,100)
  and b.job_id = (
  
  select max(a.job_id)
  From employees a
  where a.manager_id = P_MANAGER_ID);
  
  v_check_coll number;
   
    begin
      --
      if P_DEPARTMENT_NAME is null then
       return -1;
      elsif P_MANAGER_ID is null and  P_LOCATION_ID <> 1700 then
        return -2;
       elsif  P_MANAGER_ID is not null then
         --
       open c_check_coll;
       fetch c_check_coll into v_check_coll;
       close c_check_coll;
          --
            if v_check_coll = 0 then
               --
               return -3;
               -- 
            end if; 
          --      
        elsif  P_LOCATION_ID is null then
          return -4;
        ELSIF LENGTH(p_department_name) >= (p_location_id / 10) THEN
            RETURN -5;  
      end if; 
    
       --
       return 0;
       --
    end;                             
   --
   procedure add_dept (P_DEPARTMENT_NAME  departments.department_name%type,
                                P_MANAGER_ID  employees.manager_id%type,
                              P_LOCATION_ID  departments.location_id%type) is
    
   v_Result number;                           
   --
   begin
     
     v_result := dept_pack.check_new_dept(P_DEPARTMENT_NAME,P_MANAGER_ID,P_LOCATION_ID);
     
     if v_result < 0 then
      case v_result 
           when  -1 then RAISE_APPLICATION_ERROR(-20001,'department can not be emplty');
      end case;
      
      elsif v_result = 0 then
        begin
        insert into departments a (a.department_id, a.department_name, a.location_id, a.manager)
        values (dept_pack.gen_new_dept_id,P_DEPARTMENT_NAME,P_LOCATION_ID,P_MANAGER_ID);
        exception
        when others then
              RAISE_APPLICATION_ERROR(-20299, 'Insert Exception');
        end;
     end if;  
     
   
   end;                           
   --
   
   function get_dept_full_info(p_id number,
                               P_dep_emp out dep_emp_rec) return gen_info_rec is
     
   cursor c_dep_info is 
   select s.department_name, s.location_id, s.manager
   from departments s
   where s.department_id = p_id;
   
   
   cursor c_get_emp is 
   select * from employees a
   where a.department_id = p_id;
   
   v_get_emp c_get_emp%rowtype;
   
   v_dep_info c_dep_info%rowtype;
   v_return_dep gen_info_rec;
   v_index number := 1;
     begin
       
     open c_Dep_info ;
     fetch c_dep_info into v_dep_info;
     close c_dep_info;
     if v_dep_info.department_name is null then
        raise_application_error(-20001,'ddiqasd');
     end if;  
     
     v_return_dep.DEPARTMENT_NAME := v_dep_info.department_name;
     v_return_dep.MANAGER_ID := v_dep_info.manager;
     v_return_dep.LOCATION_ID := v_dep_info.location_id;
     
    FOR v_get_emp IN c_get_emp LOOP
            v_index := v_index + 1;
            P_dep_emp(v_index).employee_id := v_get_emp.employee_id;
            P_dep_emp(v_index).first_name := v_get_emp.first_name;
            P_dep_emp(v_index).last_name := v_get_emp.last_name;
            P_dep_emp(v_index).email := v_get_emp.email;
            P_dep_emp(v_index).phone_number := v_get_emp.phone_number;
            P_dep_emp(v_index).hire_date := v_get_emp.hire_date;
            P_dep_emp(v_index).job_id := v_get_emp.job_id;
            P_dep_emp(v_index).salary := v_get_emp.salary;
            P_dep_emp(v_index).commission_pct := v_get_emp.commission_pct;
            P_dep_emp(v_index).manager_id := v_get_emp.manager_id;
            P_dep_emp(v_index).department_id := v_get_emp.department_id;
        END LOOP;
    
     
     
     return v_return_dep;
     
     end;
 --------
  procedure modify_dept(p_dept_id number,
                        p_action varchar2,
                        p_dept_name varchar2) is
                        
  cursor c_check_emp is
   select count(1) as cnt
   from employees  b
   where b.department_id = p_dept_id;    
   
   v_check number;          
  begin
    
  if p_action = 'UPDATE' then
    --
    update departments a
       set a.department_name = p_dept_name
     where a.department_id = p_dept_id;
     --
  elsif p_action = 'DELETE'then
    --
    open c_check_emp;
    fetch c_check_emp into v_check;
    close c_check_emp;
    --
    if v_check > 0 then
      raise_application_error(-20001,'emp found');
    end if;
    
    delete departments a
     where a.department_id = p_dept_id;
    
    
  
  end if;  
  
  end; 


end DEPT_PACK;


